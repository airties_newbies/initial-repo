#include  <stdio.h>
#include  <stdlib.h>
#include  <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>

static int fork_execute(const char *pline)
{
        char line[1024];
	char  *argv[64];
	pid_t  pid;
	int    status;
	int i = 0;

	strcpy(line, pline);
	/* parse the command line parameters */
	while (*line != '\0') {
		while (*line == ' ' || *line == '\t' || *line == '\n') {
			line++ = '\0';
		}
		argv[i] = line;
		i++;
		while (*line != '\0' && *line != ' ' && *line != '\t' && *line != '\n') {
			line++;
		}
	}
	argv[i] = '\0';

	/* fork & execute*/
	if ((pid = fork()) < 0) {
		printf("@@@@@@@@@@@@@@@@@@@@@ pid = for failed return -1 called\n");
		return -1;
	}
	else if (pid == 0) {
		printf("@@@@@@@@@@@@@@@@@@@@@ execv called\n");
		if (execv(argv[0], argv) < 0) {
			printf("@@@@@@@@@@@@@@@@@@@@@ execv failed return -1 called\n");
			return -1;
		}
	}
	else {
		while (wait(&status) != pid);
	}

		printf("@@@@@@@@@@@@@@@@@@@@@ return 0 called\n");
	return 0;
}


int  main(void)
{
     fork_execute("/bin/ls /projects -al");
}
