#include <stdio.h>
#include <fstream>
#include <string>

using namespace std;

#define NETFLIX_CR_COPIED "/tmp/netflix_cr_copied"

int main(void)
{
	/* Read the netflix credential file name */
	std::string file = NETFLIX_CR_COPIED;
	std::ifstream is(file.c_str());
	string mEncryptedFile;
	getline(is, mEncryptedFile);
	is.close();
	remove(file.c_str());
	
	/* Move the backed-up credential file on to the active file */
	string mEncryptedFile_backup = mEncryptedFile;
	mEncryptedFile_backup += "_backup";
	printf("Copy backed up credential files on to the active ones\n");
	printf("SecureStore::load mEncryptedFile: %s\n", mEncryptedFile.c_str());
	printf("SecureStore::load mEncryptedFile_backup: %s\n", mEncryptedFile_backup.c_str());
	printf("Copy backed up credential files on to the active ones\n");
	ifstream source(mEncryptedFile_backup.c_str(), ios::binary);
	ofstream dest(mEncryptedFile.c_str(), ios::binary);
	dest << source.rdbuf();
	source.close();
	dest.close();
	remove(mEncryptedFile_backup.c_str());
}

