#include <linux/module.h>
#include <linux/skbuff.h>          
#include <linux/init.h>
#include <net/sock.h>
#include <linux/inet.h>
#include <linux/ip.h>             
#include <linux/kernel.h> 
#include <linux/netfilter.h>
#include <uapi/linux/netfilter_ipv4.h> 
 
static struct nf_hook_ops netfops;                    

unsigned int main_hook(unsigned int hooknum,
                       struct sk_buff *skb,
                       const struct net_device *in,
                       const struct net_device *out,
                       int (*okfn)(struct sk_buff*))
{
    struct iphdr *iph;
 
 	printk("%s[%d] entered\n", __FUNCTION__, __LINE__);
 	
    iph = ip_hdr(skb);
    if(iph->saddr == in_aton("192.168.2.146"))
    { 
    	printk("%s[%d] NF_DROP\n", __FUNCTION__, __LINE__);
        return NF_DROP; 
    }
    
   	printk("%s[%d] NF_ACCEPT\n", __FUNCTION__, __LINE__);
    return NF_ACCEPT;
}
 
int __init nf_module_init(void)
{
    netfops.hook              =       main_hook;
    netfops.pf                =       PF_INET;        
    netfops.hooknum           =       0;
    netfops.priority          =       NF_IP_PRI_FIRST;
    nf_register_hook(&netfops);
 
 	printk("%s[%d] entered\n", __FUNCTION__, __LINE__);
    return 0;
}
 
void  nf_module_exit(void) 
{ 
	printk("%s[%d] entered\n", __FUNCTION__, __LINE__);
    nf_unregister_hook(&netfops); 
}
 
module_init(nf_module_init);
module_exit(nf_module_exit);
MODULE_LICENSE("GPL");
