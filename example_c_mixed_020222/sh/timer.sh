#!/bin/sh

CHECK_TIME_IN_SEC=60

echo "Running $0"

#0. prepare the timer counter variables
input=$CHECK_TIME_IN_SEC
tic=`date +%s`
elap_time=0

while [ "$elap_time" -le "$input" ]; do
	echo "Checking live cd ..."

	#update timer counter
	toc=`date +%s`
	let elap_time=$toc+-$tic
	
	#wait for a while if not wait for udhcp searching
	sleep 5 &
	wait
done

exit 0
