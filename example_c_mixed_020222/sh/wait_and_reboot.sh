#!/bin/sh

echo "Running $0";

TOTAL_WAIT_TIME_DHCP=40
wait_time_dhcp=0

while true; do
	if [ $wait_time_dhcp -eq $TOTAL_WAIT_TIME_DHCP ]; then
		echo ""
		echo "-------------------------"
		echo "      ertan rebooting"
		echo "-------------------------"
		echo ""
		reboot
	fi
	sleep 1
	echo -en "\rWAITING reboot $wait_time_dhcp of $TOTAL_WAIT_TIME_DHCP"
	wait_time_dhcp=$(($wait_time_dhcp+1))
done &
