#include <stdio.h>
#include <time.h>
#include <signal.h>
#include <string.h>

timer_t gTimerid;

void upnp_wps_timer_handler(int sig) {
	printf("Catched timer signal: %d ... !!\n", sig);
	//timer_delete(gTimerid);
}

int upnp_wps_start_timer(void)
{
	struct itimerspec its;
	struct sigaction sa;
	struct sigevent sev;

	/* Create the timer */
	sa.sa_flags = SA_RESTART;
	sa.sa_handler = upnp_wps_timer_handler;
	sigemptyset(&sa.sa_mask);
	sev.sigev_notify = SIGEV_SIGNAL;
	sev.sigev_signo = SIGRTMIN;
	its.it_value.tv_sec = 2;
	its.it_interval.tv_sec = 2;
	its.it_interval.tv_nsec = 0;
	its.it_value.tv_nsec = 0;

	if(-1 == timer_create(CLOCK_REALTIME, &sev, &gTimerid))
		return -1;

	if(-1 == sigaction(SIGRTMIN, &sa, NULL))
		return -2;

	if(-1 == timer_settime(gTimerid, 0, &its, NULL))
		return -3;

	return 0;
}

int main(int ac, char **av)
{
    upnp_wps_start_timer();
    while(1);
}
